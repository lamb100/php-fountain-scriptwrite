<?php
namespace Lamb100\PHPFountainScriptwriter;
/**
 * @references: https://fountain.io/syntax https://ithub.com/mattdaly/Fountain.js
 */

class Fountain extends stdClass{
	protected $regex=[
		'title_page'=>'/^((?:title|credit|author[s]?|source|notes|draft date|date|contact|copyright)\:)/im',
		'scene_heading'=>'/^((?:\*{0,3}_?)?(?:(?:int|ext|est|i\/e)[. ]).+)|^(?:\.(?!\.+))(.+)/i',
		'scene_number'=>'/( *#(.+)# *)/',

		'transition'=>'/^((?:FADE (?:TO BLACK|OUT)|CUT TO BLACK)\.|.+ TO\:)|^(?:> *)(.+)/',

		'dialogue'=>'/^([A-Z*_]+[0-9A-Z (._\-\')]*)(\^?)?(?:\n(?!\n+))([\s\S]+)/',
		'parenthetical'=>'/^(\(.+\))$/',

		'action'=>'/^(.+)/',
		'centered'=>'/^(?:> *)(.+)(?: *<)(\n.+)*/',
		    
		'section'=>'/^(#+)(?: *)(.*)/',
		'synopsis'=>'/^(?:\=(?!\=+) *)(.*)/',

		'note'=>'/^(?:\[{2}(?!\[+))(.+)(?:\]{2}(?!\[+))$/',
		'note_inline'=>'/(?:\[{2}(?!\[+))([\s\S]+?)(?:\]{2}(?!\[+))/',
		'boneyard'=>'/(^\/\*|^\*\/)$/',

		'page_break'=>'/^\={3,}$/',
		'line_break'=>'/^ {2}$/',

		'emphasis'=>'/(_|\*{1,3}|_\*{1,3}|\*{1,3}_)(.+)(_|\*{1,3}|_\*{1,3}|\*{1,3}_)/',
		'bold_italic_underline'=>'/(_{1}\*{3}(?=.+\*{3}_{1})|\*{3}_{1}(?=.+_{1}\*{3}))(.+?)(\*{3}_{1}|_{1}\*{3})/',
		'bold_underline'=>'/(_{1}\*{2}(?=.+\*{2}_{1})|\*{2}_{1}(?=.+_{1}\*{2}))(.+?)(\*{2}_{1}|_{1}\*{2})/',
		'italic_underline'=>'/(?:_{1}\*{1}(?=.+\*{1}_{1})|\*{1}_{1}(?=.+_{1}\*{1}))(.+?)(\*{1}_{1}|_{1}\*{1})/',
		'bold_italic'=>'/(\*{3}(?=.+\*{3}))(.+?)(\*{3})/',
		'bold'=>'/(\*{2}(?=.+\*{2}))(.+?)(\*{2})/',
		'italic'=>'/(\*{1}(?=.+\*{1}))(.+?)(\*{1})/',
		'underline'=>'/(_{1}(?=.+_{1}))(.+?)(_{1})/',

		'splitter'=>'/\n{2,}/',
		'cleaner'=>'/^\n+|\n+$/',
		'standardizer'=>'/\r\n|\r/',
		'whitespacer'=>'/^\t+|^ {3,}/m',
	];
	protected $inline=[
	    'note'=>'<p class="note">$1</p>',

	    'line_break'=>'<br />',

	    'bold_italic_underline'=>'<span class=\"bold italic underline\">$2</span>',
	    'bold_underline'=>'<span class=\"bold underline\">$2</span>',
	    'italic_underline'=>'<span class=\"italic underline\">$2</span>',
	    'bold_italic'=>'<span class=\"bold italic\">$2</span>',
	    'bold'=>'<span class=\"bold\">$2</span>',
	    'italic'=>'<span class=\"italic\">$2</span>',
	    'underline'=>'<span class=\"underline\">$2</span>',
	];
	protected $pattern=[
		'title'=>[
			'title'=>'<h1>{{TEXT}}</h1>',
			'credit'=>'<p class="credit">{{TEXT}}</p>',
			'author'=>'<p class="authors">{{TEXT}}</p>',
			'authors'=>'<p class="authors">{{TEXT}}</p>',
			'source'=>'<p class="source">{{TEXT}}</p>',
			'notes'=>'<p class="notes">{{TEXT}}</p>',
			'draft_date'=>'<p class="draft-date">{{TEXT}}</p>',
			'date'=>'<p class="date">{{TEXT}}</p>',
			'contact'=>'<p class="credit">{{TEXT}}</p>',
			'copyright'=>'<p class="copywrite">{{TEXT}}</p>',
		],
		'html'=>[
			'scence_heading'=>'<h3 data-scence_no="{{SENCENO}}">{{TEXT}}</h3>',
			'transition'=>'<h2>{{TEXT}}</h2>',
			'dual_dialogue_begin'=>'<div class="dual-dialogue">{{TEXT}}</div>',
			'dialogue_begin'=>'<div class="dialogue{{DUAL}}') + '">',
			'character'=>'<h4>{{TEXT}}</h4>',
			'parenthetical'=>'<p class="parenthetical">{{TEXT}}</p>',
			'dialogue'=>'<p class="dialog-content">{{TEXT}}</p>',
			'dialogue_end'=>'</div>',
			'dual_dialogue_begin'=>'</div>',
			'section'=>'<p class="section" data-depth="{{DEPTH}}">{{TEXT}}</p>',
			'synopsis'=>'<p class="synopsis">{{TEXT}}</p>',
			'note'=>'<p class="notes">{{TEXT}}</p>',
			'boneyard_begin'=>'<p class="boneyard">',
			'boneyard_end'=>'</p>',
			'action'=>'<p>{{TEXT}}</p>',
			'centered'=>'<p class="centered">{{TEXT}}</p>',
			'page_break'=>'<hr/>',
			'line_break'=>'<br/>',
		]
	];
	protected 
		$source='',
		$parsed="";

	public function __construct($config=[]){
		foreach($config as $k=>$v){
			if($k=="regex"){
				foreach($v as $k0=>$v0){
					if(isset($this->regex[$k0])){
						$this->regex[$k0]=$v;
					}
				}
				continue;
			}
			$this->{$k}=$v;
		}
	}

	public function &load($content){
		$this->source=$content;
		return $this;
	}

	public function &loadFile($path){
		if(file_exists($path)){
			$this->source=file_get_contents($path)
		}else{
			throw new \ErrorException("file doesn't exist.");
		}
		return $this;
	}

	protected function urlExists($url){
		$aURL=parse_url($url);
		if(empty($aURL['port'])){
			$aURL['port']=80;
		}
		$intErr=0;
		$strErr='';
		$intTimeout=30;
		if(isset($aURL['host']) && $aURL['host']!=gethostbyname($aURL['host'])){
            $fid = fsockopen($aURL['host'], $aURL['port'], $intErr, $strErr, $intTimeout);
            if (!$fid) return false;
            $page = isset($aURL['path'])  ?$aURL['path']:'';
            $page .= isset($aURL['query'])?'?'.$aURL['query']:'';
            fputs($fid, 'HEAD '.$page.' HTTP/1.0'."\r\n".'Host: '.$aURL['host']."\r\n\r\n");
            $head = fread($fid, 4096);
            fclose($fid);
            return preg_match('#^HTTP/.*\s+[200|302]+\s#i', $head);
        } else {
            return false;
        }
	}

	public function &loadURL($url){
		if($this->urlExists($url)){
			$this->source=file_get_contents($url);
		}else{
			throw new \ErrorException("file doesn't exist.");
		}
		return $this;
	}

	public function &reset(){
		$this->target=$this->source;
		return $this;
	}

	protected function lexer($script=null){
		if(is_null($script)){
			$script=&$this->target;
		}
		$script=preg_replace($this->regex['boneyard'],"\n$1\n",$script);
		$script=preg_replace($this->regex['standardizer'],"\n",$script);
		$script=preg_replace($this->regex['cleaner'],"",$script);
		$script=preg_replace($this->regex['whitespacer'],"",$script);
		return $this->target=$script;
	}

	protected function lineLexer($s=null){
		if(is_null($s)){
			$s=&$this->target;
		}
		$styles=['underline', 'italic', 'bold', 'bold_italic', 'italic_underline', 'bold_underline', 'bold_italic_underline'];
		$s=preg_replace($this->regex['note_inline'],$this->inline['note'],$s);
		$s=preg_replace('/\\\*/','[star]',$s);
		$s=preg_replace('/\\_/', '[underline]',$s);
		$s=preg_replace('/\n/',$this->inline['line_break'],$s);
		foreach($styles as $style){
			$regex=$this->regex[$style];
			if(preg_match($regex,$s)){
				$s=preg_replace($regex,$this->inline,$s);
			}
		}
		return $this->target=$s=str_replace(['[star]',['underline']],['*','_'],$s);
	}

	protected function tokenize($script=null){
		if(is_null($script)){
			$script=&$this->target;
		}
		$src=preg_split($this->regex['splitter'],$this->lexer($script));
		$tokens=[];
		foreach($src as $line){
			$forPush=[];
			if(preg_match($this->regex['title_page'],$script)){
				$match=array_reverse(
					preg_split(
						$this->regex['splitter'],preg_replace(
							$this->regex['title_page'],"\n\$1",$script
						)
					)
				);
				foreach($match as $m){
					$parts=preg_split("/\:\n*/",preg_replace($this->regex['cleaner'],"",$m));
					array_push($token,[
						"type"=>str_replace(' ','_',strtolower(trim($parts[0]))),
						"text"=>trim($parts[1])
					]);
				}
				continue;
			}

			if(preg_match($this->regex['scene_heading'],$script,$match)){
				$text=($match[1]?:$match[2]);
				if(strpos($text,' ') !== (strlen($text)-2)){
					if(preg_match($this->regex['scene_number'],$text,$meta)){
						$meta=$meta[2];
						$text=preg_replace($this->regex['scene_number'],'',$text);
					}
					$forPush['type']='scene_heading';
					$forPush['text']=$text;
					if(!is_null($meta)){
						$forPush['scene_number']=$meta;
					}
					array_push($token,$forPush);
				}
				continue;
			}

			if(preg_match($this->regex['centered'],$script,$match)){
				//match[0].replace(/>|</, '')
				$text=preg_replace('/>|</','');
				$forPush['type']='centered';
				$forPush['text']=$text;
				array_push($token,$forPush);
				continue;
			}

			if(preg_match($this->regex['transition'],$script,$match)){
				$text=($match[1]?:$match[2]);
				$forPush['type']='centered';
				$forPush['text']=$text;
				array_push($token,$forPush);
				continue;
			}

			if(preg_match($this->regex['dialogue'],$script,$match)){
				if(strpos($match[1],' ')!==(strlen($match[1])-1)){
					if($match[2]){
						array_push($token,['type'=>'dual_dialogue_end']);
					}
					array_push($token,['type'=>'dialogue_end']);

					$parts=array_reverse(preg_split('/(\(.+\))(?:\n+)/',$match[3]));

					foreach($parts as $k0=>$text){
						if(strlen($text)>0){
							array_push($token,[
								"type"=>(preg_match($this->regex['parenthetical'],$text)?'parenthetical':'dialogue'),
								"text"=>$text,
							]);
						}
					}
					array_push($token,[
						'type'=>'character',
						'text'=>trim($match[1]);
					]);
					array_push($token,[
						'type'=>'dialogue_begin',
						'dual'=>(!empty($match[2])?'right':(!empty(dual)?'left':null))
					]);
					if($dual){
						array_push($token,['type'=>'dual_dialogue_begin']);
					}
					$dual=!empty($match[2]);
				}
				continue;
			}

			if(preg_match($this->regex['section'],$script,$match)){
				array_push($token,['type'=>'section','text'=>$match[2],'depth'=>strlen($match[1])]);
				continue;
			}

			if(preg_match($this->regex['synopsis'],$script,$match)){
				array_push($token,['type'=>'synopsis','text'=>$match[1]]);
				continue;
			}
			if(preg_match($this->regex['node'],$script,$match)){
				array_push($token,['type'=>'note','text'=>$match[1]]);
				continue;
			}
			if(preg_match($this->regex['boneyard'],$script,$match)){
				array_push($token,['type'=>'boneyard','text'=>$match[1]]);
				continue;
			}
			if(preg_match($this->regex['page_break'],$script)){
				array_push($token,['type'=>'page_break']);
				continue;
			}
			if(preg_match($this->regex['line_break'],$script)){
				array_push($token,['type'=>'line_break']);
				continue;
			}
		}
		return $token;
	}

	public function parse($script=null,$toks=[],$callback=null){
		if(is_null($callback)&&is_callable($toks)){
			$callback=$toks;
			$toks=null;
		}
		$tokens=$this->tokenize($script);
		$title_page=[];
		$html=[];
		$title='';
		foreach($tokens as $token){
			$token['text']=$this->lexer($token['text']);
			if($token['type']=='title'){
				$title=$token['text'];
			}
			if(isset($this->pattern['title'][$token['type']])){
				$source=$target=[];
				foreach($token as $k=>$v){
					$source[]='{{'.strtoupper($k).'}}';
					$target[]=$v;
				}
				$title_page[]=str_replace($source,$target,$this->pattern['title'][$token['type']]);
				continue;
			}
			if(isset($this->pattern['html'][$token['type']])){
				$source=$target=[];
				foreach($token as $k=>$v){
					$source[]='{{'.strtoupper($k).'}}';
					$target[]=$v;
				}
				$html[]=str_replace($source,$target,$this->pattern['html'][$token['type']]);
				continue;
			}
		}
		$output=[
			'title'=>$title,
			'html'=>[
				'title_page'=>implode('',$title_page),
				'html'=>implode('',$html),
			],
			'token'=>($tok?array_reverse($token):null)
		];
		if(is_callable($callback)){
			return $callback($output);
		}
		return $this->output=$output;
	}

	public function fountain($script,$tok,$callback=null){
		return $this->parse($script,$tok,$callback=null);
	}

	public function __toString(){
		return __CLASS__;
	}
}
?>